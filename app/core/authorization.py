from enum import Enum, unique
from typing import Type, TypeVar

from fastapi import HTTPException, status
from httpx import AsyncClient
from opentelemetry import trace

from app.core.config import settings
from app.schemas.security import AuthzRequest, AuthzResponse, OPARequest, OPAResponse, RolesRequest, RolesResponse

tracer = trace.get_tracer_provider().get_tracer(__name__)


T = TypeVar("T", bound=OPAResponse)


@unique
class RoleEnum(str, Enum):
    """
    Enumeration for the Roles in the CloWM Services.
    """

    ADMINISTRATOR: str = "administrator"
    USER: str = "user"
    REVIEWER: str = "reviewer"
    DEVELOPER: str = "developer"
    FOREIGN_USER: str = "foreign_user"
    DB_MAINTAINER: str = "db_maintainer"


def _check_role_in_role_enum(role: str) -> bool:
    try:
        RoleEnum(role)
    except ValueError:  # pragma: no cover
        return False
    return True


def map_string_to_role(roles: list[str]) -> list[RoleEnum]:
    """
    Map a list of strings to a list of Roles. Unknown roles are ignored

    Parameters
    ----------
    roles : list[str]
        The list of roles to be converted.

    Returns
    -------
    mapped_roles : list[app.core.authorization.RoleEnum]
        The mapped roles from the input.
    """
    mapped_roles = [RoleEnum(role) for role in roles if _check_role_in_role_enum(role)]
    if len(mapped_roles) == 0:
        return [RoleEnum.FOREIGN_USER]
    return mapped_roles


async def request_permission(
    request_params: AuthzRequest, client: AsyncClient, raise_error: bool = True
) -> AuthzResponse:
    """
    Send a request to OPA for an authorization policy decision.

    Parameters
    ----------
    request_params : app.schemas.security.AuthzRequest
        Input parameters for the policy decision.
    client : httpx.AsyncClient
        An async http client with an open connection.
    raise_error : bool, default True
        Raise an HTTPException if the decision of OPA is negative

    Returns
    -------
    response : app.schemas.security.AuthzResponse
        Response by OPA
    """
    with tracer.start_as_current_span(
        "authorization",
        attributes={
            "resource": request_params.resource,
            "operation": request_params.operation,
            "uid": request_params.uid,
        },
    ):
        response = await request_opa(
            client=client,
            request=OPARequest(input=request_params, opa_path="/v1/data/clowm/authz/allow"),
            response_type=AuthzResponse,
        )
    if raise_error and not response.result:  # pragma: no cover
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail=f"Action forbidden. Decision ID {response.decision_id}"
        )
    return response


async def request_roles(uids: list[str], client: AsyncClient) -> dict[str, list[RoleEnum]]:
    """
    Send a request to OPA to get all roles for a group of users.

    Parameters
    ----------
    uids : list[str]
        List of UIDs to get the roles for
    client : httpx.AsyncClient
        An async http client with an open connection.
    Returns
    -------
    mapping : dict[str, list[app.core.authorization.RoleEnum]]
        The mapping from uid to roles
    """
    with tracer.start_as_current_span("opa_request_roles", attributes={"uids": uids}):
        response = await request_opa(
            client,
            request=OPARequest(input=RolesRequest(uids=uids), opa_path="/v1/data/clowm/roles/roles"),
            response_type=RolesResponse,
        )
    return {uid: map_string_to_role(response.result.get(uid, [])) for uid in uids}


async def request_opa(client: AsyncClient, request: OPARequest, response_type: Type[T]) -> T:
    """
    Wrapper to send a generic request to OPA.

    Parameters
    ----------
    client : httpx.AsyncClient
        An async http client with an open connection.
    request : OPARequest
        The request for OPA.
    response_type : Type[OPAResponse -> T]
        The class to which the response will be parsed. T must inherit from OPAResponse.

    Returns
    -------
    response : T
        The parsed response from OPA.
    """
    with tracer.start_as_current_span(
        "opa_request",
        attributes={"opa_path": request.opa_path.strip("/"), "body": request.model_dump_json(indent=4)},
    ) as span:
        response = await client.post(
            str(settings.opa.uri) + request.opa_path.strip("/"),
            content=request.model_dump_json(),
            headers={"content-type": "application/json"},
        )
        parsed_response = response_type.model_validate_json(response.content)
        span.set_attribute("decision_id", str(parsed_response.decision_id))
        return parsed_response
