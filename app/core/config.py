import os
import secrets
from functools import cached_property
from typing import Literal, Type

from cryptography.hazmat.backends import default_backend as crypto_default_backend
from cryptography.hazmat.primitives import serialization as crypto_serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from pydantic import AnyHttpUrl, BaseModel, ByteSize, Field, FilePath, MySQLDsn, NameEmail, SecretStr, field_validator
from pydantic_settings import (
    BaseSettings,
    JsonConfigSettingsSource,
    PydanticBaseSettingsSource,
    SettingsConfigDict,
    TomlConfigSettingsSource,
    YamlConfigSettingsSource,
)


class DBSettings(BaseModel):
    port: int = Field(3306, description="Port of the database.")
    host: str = Field("localhost", description="Host of the database.")
    name: str = Field(..., description="Name of the database.")
    user: str = Field(..., description="Username in the database.")
    password: SecretStr = Field(..., description="Password for the database user.")
    verbose: bool = Field(False, description="Flag whether to print the SQL Queries in the logs.")

    @cached_property
    def dsn_sync(self) -> MySQLDsn:
        return MySQLDsn.build(
            scheme="mysql+pymysql",
            password=self.password.get_secret_value(),
            host=self.host,
            port=self.port,
            path=self.name,
            username=self.user,
        )

    @cached_property
    def dsn_async(self) -> MySQLDsn:
        return MySQLDsn.build(
            scheme="mysql+aiomysql",
            password=self.password.get_secret_value(),
            host=self.host,
            port=self.port,
            path=self.name,
            username=self.user,
        )


class S3Settings(BaseModel):
    uri: AnyHttpUrl = Field(..., description="URI of the S3 Object Storage.")
    access_key: str = Field(..., description="Access key for the S3 that owns the buckets.")
    secret_key: SecretStr = Field(..., description="Secret key for the S3 that owns the buckets.")
    username: str = Field(
        ..., description="ID of the user in ceph who owns all the buckets. Owner of 'CLOWM_S3__ACCESS_KEY'"
    )
    admin_access_key: str = Field(
        ..., description="Access key for the Ceph Object Gateway user with `user=*,bucket=*` capabilities"
    )
    admin_secret_key: SecretStr = Field(
        ..., description="Secret key for the Ceph Object Gateway user with `user=*,bucket=*` capabilities"
    )
    # 25 * 2**32 = 100 GiB
    initial_bucket_size_limit: ByteSize = Field(ByteSize(25 * 2**32), description="Size limit of the initial bucket")
    initial_bucket_object_limit: int = Field(
        10000, gt=0, lt=2**32, description="Maximum number of objects in the initial bucket"
    )

    @field_validator("initial_bucket_size_limit")
    @classmethod
    def initial_bucket_size_limit_validator(cls, size: ByteSize) -> ByteSize:
        if size.to("KiB") >= 2**32:
            raise ValueError("size can be maximal 4.3TB")
        elif size.to("KiB") < 1:
            raise ValueError("size must be at least 1 KiB")
        return size


class OPASettings(BaseModel):
    uri: AnyHttpUrl = Field(..., description="URI of the OPA Service")


class OTLPSettings(BaseModel):
    grpc_endpoint: str | None = Field(
        None, description="OTLP compatible endpoint to send traces via gRPC, e.g. Jaeger", examples=["localhost:8080"]
    )
    secure: bool = Field(False, description="Connection type")


class LifescienceOIDCSettings(BaseModel):
    client_secret: SecretStr = Field(..., description="OIDC Client secret")
    client_id: str = Field(..., description="OIDC Client ID")
    base_uri: AnyHttpUrl = Field(
        AnyHttpUrl.build(scheme="https", host="login.aai.lifescience-ri.eu"),
        description="Lifescience OIDC Base URI. Will be concatenated with `CLOWM_META_INFO_PATH`",
    )
    meta_info_path: str = Field("/oidc/.well-known/openid-configuration", description="Path to the OIDC meta data file")

    @cached_property
    def meta_info_uri(self) -> AnyHttpUrl:
        return AnyHttpUrl(str(self.base_uri) + self.meta_info_path.strip("/"))


class EmailSettings(BaseModel):
    server: str | Literal["console"] | None = Field(
        None,
        description="Hostname of SMTP server. If `console`, emails are printed to the console. If None, no emails are sent",
    )
    port: int = Field(587, description="Port of the SMTP server")
    sender_email: NameEmail = Field(
        NameEmail(email="no-reply@clowm.com", name="CloWM"), description="Email address from which the emails are sent."
    )
    reply_email: NameEmail | None = Field(None, description="Email address in the `Reply-To` header.")
    connection_security: Literal["ssl", "tls"] | Literal["starttls"] | None = Field(
        None, description="Connection security to the SMTP server."
    )
    local_hostname: str | None = Field(None, description="Overwrite the local hostname from which the emails are sent.")
    ca_path: FilePath | None = Field(None, description="Path to a custom CA certificate.")
    key_path: FilePath | None = Field(None, description="Path to the CA key.")
    user: str | None = Field(None, description="Username to use for SMTP login")
    password: SecretStr | None = Field(None, description="Password to use for SMTP login")


class Settings(BaseSettings):
    secret_key: str = Field(secrets.token_urlsafe(32), description="Secret key to sign the session cookie.")
    private_key: SecretStr | None = Field(None, description="Private RSA Key in PEM format to sign the JWTs.")
    private_key_file: FilePath | None = Field(
        None, description="Path to Private RSA Key in PEM format to sign the JWTs."
    )
    # 60 minutes * 24 hours * 8 days = 8 days
    jwt_token_expire_minutes: int = Field(60 * 24 * 8, description="JWT lifespan in minutes.")
    api_prefix: str = Field("", description="Path Prefix for all API endpoints.")
    ui_uri: AnyHttpUrl = Field(..., description="URL of the UI")
    block_foreign_users: bool = Field(False, description="Block users that have no role")

    @cached_property
    def public_key_value(self) -> SecretStr:
        private_key = crypto_serialization.load_pem_private_key(
            self.private_key_value.get_secret_value().encode("utf-8"),
            password=None,
        )
        return SecretStr(
            private_key.public_key()
            .public_bytes(crypto_serialization.Encoding.PEM, crypto_serialization.PublicFormat.SubjectPublicKeyInfo)
            .decode("UTF-8")
        )

    @cached_property
    def private_key_value(self) -> SecretStr:
        private_key = ""
        if self.private_key is not None:
            private_key = self.private_key.get_secret_value()
        if self.private_key_file is not None:
            with open(self.private_key_file) as f:
                private_key = f.read()
        if len(private_key) == 0:
            key = rsa.generate_private_key(backend=crypto_default_backend(), public_exponent=65537, key_size=2048)
            private_key = key.private_bytes(
                crypto_serialization.Encoding.PEM,
                crypto_serialization.PrivateFormat.PKCS8,
                crypto_serialization.NoEncryption(),
            ).decode("UTF-8")
            print("No private key provided. Use randomly generated RSA key")
            print(
                key.public_key()
                .public_bytes(crypto_serialization.Encoding.PEM, crypto_serialization.PublicFormat.SubjectPublicKeyInfo)
                .decode("UTF-8")
            )
        return SecretStr(private_key)

    db: DBSettings
    s3: S3Settings
    opa: OPASettings
    email: EmailSettings = EmailSettings()
    otlp: OTLPSettings = OTLPSettings()
    lifescience_oidc: LifescienceOIDCSettings

    model_config = SettingsConfigDict(
        env_prefix="CLOWM_",
        env_file=".env",
        extra="ignore",
        secrets_dir="/run/secrets" if os.path.isdir("/run/secrets") else None,
        env_nested_delimiter="__",
        yaml_file=os.getenv("CLOWM_CONFIG_FILE_YAML", "config.yaml"),
        toml_file=os.getenv("CLOWM_CONFIG_FILE_TOML", "config.toml"),
        json_file=os.getenv("CLOWM_CONFIG_FILE_JSON", "config.json"),
    )

    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: Type[BaseSettings],
        init_settings: PydanticBaseSettingsSource,
        env_settings: PydanticBaseSettingsSource,
        dotenv_settings: PydanticBaseSettingsSource,
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> tuple[PydanticBaseSettingsSource, ...]:
        return (
            env_settings,
            file_secret_settings,
            dotenv_settings,
            YamlConfigSettingsSource(settings_cls),
            TomlConfigSettingsSource(settings_cls),
            JsonConfigSettingsSource(settings_cls),
        )


settings = Settings()
