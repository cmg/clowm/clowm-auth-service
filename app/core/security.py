from datetime import UTC, datetime, timedelta
from enum import Enum, unique

from authlib.integrations.starlette_client import OAuth, StarletteOAuth2App
from authlib.jose import JsonWebToken

from app.core.authorization import RoleEnum
from app.core.config import settings

ISSUER = "clowm"
ALGORITHM = "RS256"
jwt = JsonWebToken([ALGORITHM])


def create_access_token(subject: str, roles: list[RoleEnum]) -> str:
    """
    Create a JWT access token.

    Parameters
    ----------
    subject : str
        Ths subject in the JWT.
    roles : list[app.core.authorization.RoleEnum]
        List of roles that the user has

    Returns
    -------
    token : str
        The generated JWT.
    """
    expire = datetime.now(UTC) + timedelta(minutes=settings.jwt_token_expire_minutes)
    to_encode = {"exp": expire, "sub": subject, "iss": ISSUER, "roles": [role.value for role in roles]}
    encoded_jwt = jwt.encode(
        header={"alg": ALGORITHM}, payload=to_encode, key=settings.private_key_value.get_secret_value()
    )
    return encoded_jwt.decode("utf-8")


def decode_token(token: str) -> dict[str, str]:
    """
    Decode and verify a JWT token.

    Parameters
    ----------
    token : str
        The JWT to decode.

    Returns
    -------
    decoded_token : dict[str, str]
        Payload of the decoded token.
    """
    claims = jwt.decode(
        s=token,
        key=settings.public_key_value.get_secret_value(),
        claims_options={
            "iss": {"essential": True},
            "sub": {"essential": True},
            "exp": {"essential": True},
            "roles": {"essential": True},
        },
    )
    claims.validate()
    return claims


@unique
class OIDCProvider(str, Enum):
    lifescience = "lifescience"


_oauth = OAuth()
_oauth.register(
    name=OIDCProvider.lifescience.name,
    client_id=settings.lifescience_oidc.client_id,
    client_secret=settings.lifescience_oidc.client_secret.get_secret_value(),
    server_metadata_url=str(settings.lifescience_oidc.meta_info_uri),
    client_kwargs={"scope": "openid profile email", "code_challenge_method": "S256"},
)


def get_provider(provider: OIDCProvider) -> StarletteOAuth2App:  # pragma: no cover
    return getattr(_oauth, provider.name)
