from datetime import datetime
from typing import Any
from uuid import UUID

from pydantic import BaseModel, Field


class OPAResponse(BaseModel):
    decision_id: UUID = Field(
        ...,
        description="Decision ID for for the specific decision",
        examples=[UUID(hex="8851dce0-7546-4e81-a89d-111cbec376c1")],
    )


class OPARequest(BaseModel):
    opa_path: str = Field(
        "/v1/data/clowm/authz/allow",
        description="URL path where OPA is queried",
        exclude=True,
        examples=["/v1/data/clowm/authz/allow"],
    )
    input: Any = Field(..., description="Out wrapper for the OPA request")


class AuthzResponse(OPAResponse):
    """Schema for an authz response from OPA"""

    result: bool = Field(..., description="Result of the Authz request")


class AuthzRequest(BaseModel):
    """Schema for an authz Request to OPA"""

    uid: str = Field(..., description="UID of user", examples=["28c5353b8bb34984a8bd4169ba94c606"])
    operation: str = Field(..., description="Operation the user wants to perform", examples=["read"])
    resource: str = Field(..., description="Resource the operation should be performed on", examples=["bucket"])


class RolesRequest(BaseModel):
    uids: list[str] = Field(..., description="UIDs of user to get roles for", examples=[["abc", "dfg"]])


class RolesResponse(OPAResponse):
    result: dict[str, list[str]]


class JWT(BaseModel):
    """
    Schema for a JWT. Only for convenience
    """

    exp: datetime
    sub: str
    raw_token: str


class ErrorDetail(BaseModel):
    """
    Schema for a error due to a rejected request.
    """

    detail: str = Field(..., description="Detail about the occurred error")
