from uuid import UUID

from clowmdb.models import User as UserDB
from pydantic import BaseModel, ConfigDict, Field

from app.core.authorization import RoleEnum


class User(BaseModel):
    """
    Schema for a user.
    """

    uid: UUID = Field(
        ...,
        description="ID of the user",
        examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
    )
    display_name: str = Field(
        ...,
        description="Full Name of the user",
        examples=["Bilbo Baggins"],
        max_length=256,
    )
    roles: list[RoleEnum] | None = Field(None, description="Roles of the user", examples=[[RoleEnum.USER]])

    model_config = ConfigDict(from_attributes=True)

    @staticmethod
    def from_db_user(user: UserDB, **kwargs) -> "User":  # type: ignore[no-untyped-def]
        return User(uid=user.uid, display_name=user.display_name, **kwargs)
