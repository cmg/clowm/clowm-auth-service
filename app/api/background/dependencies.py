from contextlib import asynccontextmanager
from typing import AsyncIterator

from sqlalchemy.ext.asyncio import AsyncSession

from app.api import dependencies


@asynccontextmanager
async def get_background_db() -> AsyncIterator[AsyncSession]:
    async for db in dependencies.get_db():
        yield db
        break
