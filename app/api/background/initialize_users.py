import json
from uuid import uuid4

from clowmdb.models import Bucket, User
from opentelemetry import trace

from app.api import dependencies
from app.api.background.dependencies import get_background_db
from app.core.config import settings
from app.crud import CRUDBucket, CRUDUser

tracer = trace.get_tracer_provider().get_tracer(__name__)

cors_rule = {
    "CORSRules": [
        {
            "ID": "websiteaccess",
            "AllowedHeaders": [
                "amz-sdk-invocation-id",
                "amz-sdk-request",
                "authorization",
                "content-type",
                "x-amz-content-sha256",
                "x-amz-copy-source",
                "x-amz-date",
                "x-amz-user-agent",
                "content-md5",
            ],
            "AllowedMethods": ["GET", "PUT", "POST", "DELETE"],
            "AllowedOrigins": [str(settings.ui_uri).strip("/")],
            "ExposeHeaders": [
                "Etag",
            ],
            "MaxAgeSeconds": 100,
        },
    ]
}


async def initialize_user(user: User) -> None:
    """
    Create an initial bucket for a user/

    Parameters
    ----------
    user : clowmdb.models.User
        The user who needs to be initialized.
    """
    async with get_background_db() as db:
        name = "initial-bucket-" + user.uid.hex[-8:]
        while (await CRUDBucket.get(name, db=db)) is not None:
            name = "initial-bucket-" + uuid4().hex[:8]  # pragma: no cover
        bucket = Bucket(
            name=name,
            description=f"Initial bucket for {user.display_name}",
            owner_id_bytes=user.uid_bytes,
            size_limit=int(settings.s3.initial_bucket_size_limit.to("KiB")),
            object_limit=settings.s3.initial_bucket_object_limit,
        )
        await CRUDBucket.create(bucket, db=db)

    s3 = dependencies.get_s3_resource()
    s3_bucket = s3.Bucket(bucket.name)
    with tracer.start_as_current_span("s3_create_bucket", attributes={"bucket_name": bucket.name}):
        s3_bucket.create(ObjectOwnership="BucketOwnerEnforced")
    # Add basic permission to the user.
    bucket_policy = json.dumps(
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "ProxyOwnerPerm",
                    "Effect": "Allow",
                    "Principal": {"AWS": [f"arn:aws:iam:::user/{settings.s3.username}"]},
                    "Action": ["s3:GetObject"],
                    "Resource": [f"arn:aws:s3:::{bucket.name}/*"],
                },
                {
                    "Sid": "PseudoOwnerPerm",
                    "Effect": "Allow",
                    "Principal": {"AWS": [f"arn:aws:iam:::user/{str(user.uid)}"]},
                    "Action": ["s3:GetObject", "s3:DeleteObject", "s3:PutObject", "s3:ListBucket"],
                    "Resource": [f"arn:aws:s3:::{bucket.name}/*", f"arn:aws:s3:::{bucket.name}"],
                },
            ],
        }
    )

    with tracer.start_as_current_span(
        "s3_put_bucket_policy", attributes={"bucket_name": bucket.name, "policy": bucket_policy}
    ):
        s3_bucket.Policy().put(Policy=bucket_policy)
    with tracer.start_as_current_span(
        "s3_put_bucket_cors_rules", attributes={"bucket_name": bucket.name, "rules": json.dumps(cors_rule)}
    ):
        # Add CORS rule to bucket to allow access from the browser
        s3_bucket.Cors().put(CORSConfiguration=cors_rule)  # type: ignore[arg-type]

    rgw = dependencies.get_rgw_admin()
    with tracer.start_as_current_span("rgw_set_bucket_quota", attributes={"bucket_name": bucket.name}):
        rgw.set_bucket_quota(
            uid=str(user.uid),
            bucket=bucket.name,
            max_objects=bucket.object_limit,
            max_size_kb=bucket.size_limit,
            enabled=True,
        )

    async with get_background_db() as db:
        await CRUDUser.mark_initialized(user.uid, db=db)
