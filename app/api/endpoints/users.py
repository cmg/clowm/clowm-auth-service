from typing import Any, Awaitable, Callable
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException, Path, Query, status
from opentelemetry import trace
from pydantic.json_schema import SkipJsonSchema
from typing_extensions import Annotated

from app.api.dependencies import AuthorizationDependency, CurrentUser, DBSession, GetRolesFunction
from app.core.authorization import RoleEnum
from app.crud import CRUDUser
from app.otlp import start_as_current_span_async
from app.schemas.user import User as UserSchema

router = APIRouter(prefix="/users", tags=["User"])

user_authorization = AuthorizationDependency(resource="user")
Authorization = Annotated[Callable[[str], Awaitable[Any]], Depends(user_authorization)]
tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("/me", response_model=UserSchema, summary="Get the logged in user")
@start_as_current_span_async("api_get_logged_in_user", tracer=tracer)
async def get_logged_in_user(
    current_user: CurrentUser, get_roles: GetRolesFunction, request_opa: Authorization
) -> UserSchema:
    """
    Return the user associated with the used JWT.
    Permission `user:read` required.
    \f
    Parameters
    ----------
    current_user : clowmdb.models.User
        User from the database associated to the used JWT. Dependency injection.
    request_opa : Callable[[AuthzRequest], Awaitable[AuthzResponse]]
        Async function to call which request OPA for a policy decision. Dependency Injection.
    get_roles : Callable[[list[str]], Awaitable[dict[str, list[RoleEnum]]]]
        Async function to call which request the LDAP to get the roles for multiple UIDs. Dependency Injection.

    Returns
    -------
    current_user : clowmdb.models.User
        User associated to used JWT.
    """
    await request_opa("read")
    roles = (await get_roles([current_user.lifescience_id]))[current_user.lifescience_id]
    return UserSchema.from_db_user(current_user, roles=roles)


@router.get("", response_model=list[UserSchema], summary="List users and search by their name")
@start_as_current_span_async("api_list_users", tracer=tracer)
async def list_users(
    db: DBSession,
    get_roles: GetRolesFunction,
    request_opa: Authorization,
    name_substring: Annotated[
        str | SkipJsonSchema[None],
        Query(
            min_length=3,
            max_length=30,
            description="Filter users by a substring in their name. Permission `user:search` required",
        ),
    ] = None,
    filter_roles: Annotated[
        list[RoleEnum] | SkipJsonSchema[None],
        Query(
            description="Filter users by their role. If multiple are selected, they are concatenating by an OR Expression. Permission `user:read_any` required"  # noqa:E501
        ),
    ] = None,
    include_roles: Annotated[
        bool,
        Query(
            description="Flag whether to include the roles of the users in the response. If True, permission `user:read_any` required."  # noqa:E501
        ),
    ] = False,
) -> list[UserSchema]:
    """
    Return the users that have a specific substring in their name.

    Permission `user:read_any` required, except when `name_substring` as only query parameter is set,
    then permission `user:search` required.
    \f
    Parameters
    ----------
    name_substring : string | None, default None
        Filter users by a substring in their name. Query Parameter.
    filter_roles : list[app.core.authorization.RoleEnum] | None, default None
        Filter users by their role. Query Parameter.
    include_roles : bool, default False
        Flag whether to include the roles of the users in the response. Query Parameter.
    request_opa : Callable[[AuthzRequest], Awaitable[AuthzResponse]]
        Async Function to call which request OPA for a policy decision. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    get_roles : Callable[[list[str]], Awaitable[dict[str, list[RoleEnum]]]]
        Async function to call which request the LDAP to get the roles for multiple UIDs. Dependency Injection.

    Returns
    -------
    users: list[clowmdb.models.User]
        Users who have the substring in their name.
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("include_roles", include_roles)
    if name_substring is not None:  # pragma: no cover
        current_span.set_attribute("name_substring", name_substring)
    if filter_roles is not None and len(filter_roles) > 0:  # pragma: no cover
        current_span.set_attribute("filter_roles", [role.name for role in filter_roles])

    await request_opa(
        "search" if name_substring is not None and filter_roles is None and not include_roles else "read_any"
    )

    users = await CRUDUser.list_users(name_substring=name_substring, db=db)
    user_to_role_mapping = await get_roles([u.lifescience_id for u in users])
    if filter_roles is not None and len(filter_roles) > 0:
        filter_roles_set = set(filter_roles)
        users = [
            user for user in users if len(filter_roles_set.intersection(user_to_role_mapping[user.lifescience_id])) > 0
        ]
    return [
        UserSchema.from_db_user(u, roles=user_to_role_mapping[u.lifescience_id] if include_roles else None)
        for u in users
    ]


@router.get("/{uid}", response_model=UserSchema, summary="Get a user by its uid")
@start_as_current_span_async("api_get_user", tracer=tracer)
async def get_user(
    db: DBSession,
    get_roles: GetRolesFunction,
    request_opa: Authorization,
    uid: Annotated[
        UUID,
        Path(description="UID of a user", examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"]),
    ],
    include_roles: Annotated[
        bool,
        Query(
            description="Flag whether to include the roles of the users in the response. If True, permission `user:read_any` required.",  # noqa:E501
        ),
    ] = False,
) -> UserSchema:
    """
    Return the user with the specific uid.

    Permission `user:read` required if the current user has the same uid as `uid` otherwise `user:read_any` required.
    \f
    Parameters
    ----------
    uid : uuid.UUID
        The uid of a user. URL path parameter.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    include_roles : bool, default False
        Flag whether to include the roles of the user in the response. Query Parameter.
    request_opa : Callable[[AuthzRequest], Awaitable[AuthzResponse]]
        Async Function to call which request OPA for a policy decision. Dependency Injection.
    get_roles : Callable[[list[str]], Awaitable[dict[str, list[RoleEnum]]]]
        Async function to call which request the LDAP to get the roles for multiple UIDs. Dependency Injection.

    Returns
    -------
    user : clowmdb.models.User
        User with given uid.
    """
    trace.get_current_span().set_attributes({"uid": str(uid), "include_roles": include_roles})
    await request_opa("read_any" if include_roles else "read")
    user = await CRUDUser.get(uid, db=db)
    if user is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"User with id '{str(uid)}' not found")
    roles = (await get_roles([user.lifescience_id]))[user.lifescience_id] if include_roles else None
    return UserSchema.from_db_user(user, roles=roles)
