from functools import partial
from typing import TYPE_CHECKING, Annotated, Any, AsyncGenerator, Awaitable, Callable
from uuid import UUID

from authlib.integrations.base_client.errors import OAuthError
from authlib.jose.errors import BadSignatureError, DecodeError, ExpiredTokenError
from clowmdb.db.session import get_async_session
from clowmdb.models import User
from fastapi import Depends, HTTPException, Path, status
from fastapi.requests import Request
from fastapi.security import HTTPBearer
from fastapi.security.http import HTTPAuthorizationCredentials
from httpx import AsyncClient
from opentelemetry import trace
from rgwadmin import RGWAdmin
from sqlalchemy.ext.asyncio import AsyncSession

from app.ceph.rgw import rgw, s3_resource
from app.core import security
from app.core.authorization import RoleEnum, request_permission, request_roles
from app.core.config import settings
from app.crud import CRUDUser
from app.otlp import start_as_current_span_async
from app.schemas.security import JWT, AuthzRequest, AuthzResponse

if TYPE_CHECKING:
    from mypy_boto3_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

bearer_token = HTTPBearer(description="JWT Token", auto_error=False)
tracer = trace.get_tracer_provider().get_tracer(__name__)


class LoginException(Exception):
    def __init__(self, error_source: str):
        self.error_source = error_source


def get_rgw_admin() -> RGWAdmin:  # pragma: no cover
    return rgw


RGWService = Annotated[RGWAdmin, Depends(get_rgw_admin)]


def get_s3_resource() -> S3ServiceResource:  # pragma: no cover
    return s3_resource


S3Service = Annotated[S3ServiceResource, Depends(get_s3_resource)]


async def get_db() -> AsyncGenerator[AsyncSession, None]:  # pragma: no cover
    """
    Get a Session with the database.

    FastAPI Dependency Injection Function.

    Returns
    -------
    db : AsyncGenerator[AsyncSession, None]
        Async session object with the database
    """
    async with get_async_session(str(settings.db.dsn_async), verbose=settings.db.verbose) as db:
        yield db


DBSession = Annotated[AsyncSession, Depends(get_db)]


async def get_http_client(request: Request) -> AsyncClient:  # pragma: no cover
    # Fetch open http client from the app
    return request.app.requests_client


HTTPClient = Annotated[AsyncClient, Depends(get_http_client)]


@start_as_current_span_async("decode_jwt", tracer=tracer)
async def decode_bearer_token(
    db: DBSession,
    token: HTTPAuthorizationCredentials | None = Depends(bearer_token),
) -> JWT:
    """
    Get the decoded JWT or reject request if it is not valid.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    token : fastapi.security.http.HTTPAuthorizationCredentials
        Bearer token sent with the HTTP request. Dependency Injection.

    Returns
    -------
    token : app.schemas.security.JWT
        The verified and decoded JWT.
    """
    if token is None:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated")
    try:
        jwt = JWT(**security.decode_token(token.credentials), raw_token=token.credentials)
        trace.get_current_span().set_attributes({"exp": jwt.exp.isoformat(), "uid": jwt.sub})
        await get_current_user(db, jwt)  # make sure the user exists
        return jwt
    except ExpiredTokenError:  # pragma: no cover
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="JWT Signature has expired")
    except (DecodeError, BadSignatureError):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Malformed JWT")


BearerToken = Annotated[JWT, Depends(decode_bearer_token)]


async def get_current_user(db: DBSession, token: BearerToken) -> User:
    """
    Get the current user from the database based on the JWT.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    token : app.schemas.security.JWT
        The verified and decoded JWT.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    user : clowmdb.models.User
        User associated with the JWT sent with the HTTP request.
    """
    try:
        uid = UUID(token.sub)
    except ValueError:  # pragma: no cover
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Malformed JWT")
    user = await CRUDUser.get(uid, db=db)
    if user:
        return user
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")


CurrentUser = Annotated[User, Depends(get_current_user)]


class AuthorizationDependency:
    """
    Class to parameterize the authorization request with the resource to perform an operation on.
    """

    def __init__(self, resource: str):
        """
        Parameters
        ----------
        resource : str
            Resource parameter for the authorization requests
        """
        self.resource = resource

    def __call__(
        self,
        user: CurrentUser,
        client: HTTPClient,
    ) -> Callable[[str], Awaitable[AuthzResponse]]:
        """
        Get the function to request the authorization service with the resource, JWT and HTTP Client already injected.

        Parameters
        ----------
        user : clowmdb.models.User
            The current user based on the JWT. Dependency Injection.
        client : httpx.AsyncClient
            HTTP Client with an open connection. Dependency Injection.

        Returns
        -------
        authorization_function : Callable[[str], Awaitable[app.schemas.security.AuthzResponse]]
            Async function which ask the Auth service for authorization. It takes the operation as the only input.
        """

        async def authorization_wrapper(operation: str) -> AuthzResponse:
            params = AuthzRequest(operation=operation, resource=self.resource, uid=user.lifescience_id)
            return await request_permission(request_params=params, client=client)

        return authorization_wrapper


async def get_opa_roles_request(
    client: HTTPClient,
) -> Callable[[list[str]], Awaitable[dict[str, list[RoleEnum]]]]:
    """
    Provide a wrapper around the `request_roles` method and inject an async http client automatically.

    Returns
    -------
    request_roles: Callable[[list[str]], Awaitable[dict[str, list[RoleEnum]]]]
        Callable async function to request the roles for multiple UIDs
    """
    return partial(request_roles, client=client)


GetRolesFunction = Annotated[
    Callable[[list[str]], Awaitable[dict[str, list[RoleEnum]]]], Depends(get_opa_roles_request)
]


async def verify_and_fetch_userinfo(
    request: Request,
    provider: Annotated[security.OIDCProvider, Path(title="The OIDC provider from which the callback originates")],
) -> dict[str, Any]:  # pragma: no cover
    try:
        if "error" in request.query_params.keys():
            # if there is an error in the login flow, like a canceled login request, then notify the client
            raise LoginException(error_source=request.query_params["error"])
        with tracer.start_as_current_span("oidc_authorize_access_token"):
            claims = await security.get_provider(provider).authorize_access_token(request)
        # ID token doesn't have all necessary information, call userinfo endpoint
        with tracer.start_as_current_span("oidc_get_user_info"):
            return await security.get_provider(provider).userinfo(token=claims)
    except OAuthError:
        # if there is an error in the oauth flow, like an expired token, then notify the client
        raise LoginException(error_source="oidc")
    except AttributeError:
        raise LoginException(error_source=f"unknown_provider {provider.name}")
