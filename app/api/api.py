from typing import Any

from fastapi import APIRouter, Depends, status

from app.api.dependencies import decode_bearer_token
from app.api.endpoints import login, users
from app.schemas.security import ErrorDetail

alternative_responses: dict[int | str, dict[str, Any]] = {
    status.HTTP_400_BAD_REQUEST: {
        "model": ErrorDetail,
        "description": "Error decoding JWT Token",
        "content": {"application/json": {"example": {"detail": "Malformed JWT Token"}}},
    },
    status.HTTP_401_UNAUTHORIZED: {
        "model": ErrorDetail,
        "description": "Not Authenticated",
        "content": {"application/json": {"example": {"detail": "Not authenticated"}}},
    },
    status.HTTP_403_FORBIDDEN: {
        "model": ErrorDetail,
        "description": "Not Authorized",
        "content": {
            "application/json": {
                "example": {"detail": "Action Forbidden. Decision ID 187d1324-312d-4723-85d4-3ba56072e50e"}
            }
        },
    },
    status.HTTP_404_NOT_FOUND: {
        "model": ErrorDetail,
        "description": "Entity not Found",
        "content": {"application/json": {"example": {"detail": "Entity not found."}}},
    },
}

api_router = APIRouter()
api_router.include_router(login.router)
api_router.include_router(
    users.router,
    dependencies=[Depends(decode_bearer_token)],
    responses=alternative_responses,
)
