import logging

import httpx
from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

from app.core.config import settings

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

max_tries = 30  # 30 seconds
wait_seconds = 2


@retry(
    stop=stop_after_attempt(max_tries),
    wait=wait_fixed(wait_seconds),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
def init() -> None:
    try:
        httpx.get(str(settings.s3.uri), timeout=5.0)
    except Exception as e:
        logger.error(e)
        raise e


def main() -> None:
    logger.info("Check Ceph connection")
    init()
    logger.info("Ceph connection established")


if __name__ == "__main__":
    main()
