from typing import Sequence
from uuid import UUID

from clowmdb.models import User
from opentelemetry import trace
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from app.otlp import start_as_current_span_async

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDUser:
    @staticmethod
    @start_as_current_span_async("db_create_user", tracer=tracer)
    async def create(user: User, *, db: AsyncSession) -> User:
        """
        Create a new user in the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        user : clowmdb.models.User
            The user to create.

        Returns
        -------
        user : clowmdb.models.User
            The newly created user.
        """
        trace.get_current_span().set_attributes(
            {"lifescience_id": user.lifescience_id, "display_name": user.display_name}
        )
        db.add(user)
        await db.commit()
        return user

    @staticmethod
    async def update_email(uid: UUID, email: str | None = None, *, db: AsyncSession) -> None:
        stmt = update(User).where(User.uid_bytes == uid.bytes).values(email=email)
        with tracer.start_as_current_span(
            "db_update_user_email", attributes={"sql_query": str(stmt), "uid": str(uid), "email": str(email)}
        ):
            await db.execute(stmt)
            await db.commit()

    @staticmethod
    async def mark_initialized(uid: UUID, *, db: AsyncSession) -> None:
        stmt = update(User).where(User.uid_bytes == uid.bytes).values(initialized=True)
        with tracer.start_as_current_span(
            "db_mark_user_initialized", attributes={"sql_query": str(stmt), "uid": str(uid)}
        ):
            await db.execute(stmt)
            await db.commit()

    @staticmethod
    async def get(uid: UUID, *, db: AsyncSession) -> User | None:
        """
        Get a user by its UID.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        uid : uuid.UUID
            UID of a user.

        Returns
        -------
        user : clowmdb.models.User | None
            The user for the given UID if he exists, None otherwise
        """
        stmt = select(User).where(User.uid_bytes == uid.bytes)
        with tracer.start_as_current_span("db_get_user", attributes={"sql_query": str(stmt), "uid": str(uid)}):
            return await db.scalar(stmt)

    @staticmethod
    async def get_by_lifescience_id(lifescience_id: str, *, db: AsyncSession) -> User | None:
        """
        Get a user by its lifescience_id.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        lifescience_id : str
            Lifescience ID of a user

        Returns
        -------
        user : clowmdb.models.User | None
            The user for the given lifescience_id if he exists, None otherwise
        """
        stmt = select(User).where(User.lifescience_id == lifescience_id)
        with tracer.start_as_current_span(
            "db_get_user_by_lifescience_id", attributes={"sql_query": str(stmt), "lifescience_id": str(lifescience_id)}
        ):
            return await db.scalar(stmt)

    @staticmethod
    async def list_users(name_substring: str | None = None, *, db: AsyncSession) -> Sequence[User]:
        """
        Search for users that contain a specific substring in their name.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        name_substring : str
            Substring to search for in the name of a user.

        Returns
        -------
        users : list[clowmdb.models.User]
            List of users which have the given substring in their name.
        """
        with tracer.start_as_current_span("db_list_users") as span:
            stmt = select(User)
            if name_substring is not None:
                span.set_attribute("name_substring", name_substring)
                stmt = stmt.where(User.display_name.contains(name_substring))
            span.set_attribute("sql_query", str(stmt))
            return (await db.scalars(stmt)).all()
