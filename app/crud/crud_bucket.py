from clowmdb.models import Bucket
from opentelemetry import trace
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.otlp import start_as_current_span_async

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDBucket:
    @staticmethod
    async def get(bucket_name: str, *, db: AsyncSession) -> Bucket | None:
        """
        Get a bucket by its name.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        bucket_name : str
            The name of the bucket

        Returns
        -------
        bucket : clowmdb.models.Bucket
            Bucket if it exists, None otherwise
        """
        with tracer.start_as_current_span("db_get_bucket", attributes={"bucket_name": bucket_name}) as span:
            stmt = select(Bucket).where(Bucket.name == bucket_name)
            span.set_attribute("sql_query", str(stmt))
            bucket = await db.execute(stmt)
        return bucket.scalar()

    @staticmethod
    @start_as_current_span_async("db_create_bucket", tracer=tracer)
    async def create(bucket: Bucket, *, db: AsyncSession) -> Bucket:
        """
        Create a new bucket in the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        bucket : clowmdb.model.Bucket
            The bucket to create.

        Returns
        -------
        bucket : clowmdb.models.Bucket
            The newly created bucket.
        """
        trace.get_current_span().set_attributes({"bucket_in": str(bucket)})
        db.add(bucket)
        await db.commit()
        return bucket
