from typing import TYPE_CHECKING

from boto3 import resource
from rgwadmin import RGWAdmin

from app.core.config import settings

if TYPE_CHECKING:
    from mypy_boto3_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

s3_resource: S3ServiceResource = resource(
    service_name="s3",
    endpoint_url=str(settings.s3.uri).strip("/"),
    aws_access_key_id=settings.s3.access_key,
    aws_secret_access_key=settings.s3.secret_key.get_secret_value(),
    verify=settings.s3.uri.scheme == "https",
)
rgw = RGWAdmin(
    access_key=settings.s3.admin_access_key,
    secret_key=settings.s3.admin_secret_key.get_secret_value(),
    secure=settings.s3.uri.scheme == "https",
    server=str(settings.s3.uri).split("://")[-1].strip("/"),
)
