import asyncio
from typing import Any, AsyncIterator, Generator

import httpx
import pytest
import pytest_asyncio
from clowmdb.db.session import get_async_session
from clowmdb.models import Bucket, User
from fastapi import status
from fastapi.responses import RedirectResponse
from sqlalchemy.ext.asyncio import AsyncSession

from app.api import dependencies
from app.api.dependencies import (
    LoginException,
    get_db,
    get_http_client,
    get_rgw_admin,
    get_s3_resource,
    verify_and_fetch_userinfo,
)
from app.core import security
from app.core.config import settings
from app.main import app
from app.tests.mocks import MockOpa, MockRGWAdmin, MockS3ServiceResource
from app.tests.utils.user import create_random_user, get_authorization_headers, random_lower_string
from app.tests.utils.utils import CleanupList


@pytest.fixture(scope="session")
def event_loop() -> Generator:
    """
    Creates an instance of the default event loop for the test session.
    """
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def mock_rgw_admin() -> MockRGWAdmin:
    """
    Fixture for creating a mock object for the rgwadmin package.
    """
    return MockRGWAdmin()


@pytest.fixture(scope="session")
def mock_s3_service() -> MockS3ServiceResource:
    """
    Fixture for creating a mock object for the rgwadmin package.
    """
    return MockS3ServiceResource()


@pytest.fixture(scope="session")
def mock_opa_service() -> MockOpa:
    """
    Fixture for creating a mock object for the rgwadmin package.
    """
    return MockOpa()


@pytest.fixture(autouse=True)
def monkeypatch_background_dependencies(
    monkeypatch: pytest.MonkeyPatch, mock_s3_service: MockS3ServiceResource, mock_rgw_admin: MockRGWAdmin
) -> None:
    """
    Fixture to eliminate the dependency of a mock OIDC service
    """

    class MockProvider:
        async def authorize_redirect(self, *args: Any, **kwargs: Any) -> RedirectResponse:
            return RedirectResponse(
                url=str(settings.lifescience_oidc.base_uri),
                status_code=status.HTTP_302_FOUND,
            )

    monkeypatch.setattr(security, "get_provider", lambda x: MockProvider())
    monkeypatch.setattr(dependencies, "get_rgw_admin", lambda: mock_rgw_admin)
    monkeypatch.setattr(dependencies, "get_s3_resource", lambda: mock_s3_service)


@pytest_asyncio.fixture(scope="module")
async def client(
    mock_rgw_admin: MockRGWAdmin,
    mock_s3_service: MockS3ServiceResource,
    mock_opa_service: MockOpa,
    db: AsyncSession,
) -> AsyncIterator[httpx.AsyncClient]:
    """
    Fixture for creating a TestClient and perform HTTP Request on it.
    Overrides the dependency for the RGW admin operations.
    """

    async def get_mock_userinfo(sub: str, name: str, email: str = "", error: bool = False) -> dict[str, str]:
        if error:
            raise LoginException(error_source="mock_error")
        return {"sub": sub + "@lifescience-ri.eu", "name": name, "email": email}

    async def get_mock_http_client() -> AsyncIterator[httpx.AsyncClient]:
        """
        FastAPI Dependency to get an async httpx client with mock transport.

        Returns
        -------
        client : AsyncIterator[httpx.AsyncClient]
            Http client with mock transports.
        """

        async with httpx.AsyncClient(
            transport=httpx.MockTransport(lambda request: mock_opa_service.handle_request(request))
        ) as http_client:
            yield http_client

    app.dependency_overrides[get_rgw_admin] = lambda: mock_rgw_admin
    app.dependency_overrides[get_s3_resource] = lambda: mock_s3_service
    app.dependency_overrides[verify_and_fetch_userinfo] = get_mock_userinfo
    app.dependency_overrides[get_http_client] = get_mock_http_client
    app.dependency_overrides[get_db] = lambda: db
    async with httpx.AsyncClient(transport=httpx.ASGITransport(app=app), base_url="http://localhost") as ac:  # type: ignore[arg-type]
        yield ac
    app.dependency_overrides = {}


@pytest_asyncio.fixture(scope="module")
async def user_token_headers(random_user: User) -> dict[str, str]:
    """
    Create valid authorization header with a successful login.
    """
    return get_authorization_headers(uid=random_user.uid)


@pytest_asyncio.fixture(scope="module")
async def db() -> AsyncIterator[AsyncSession]:
    """
    Fixture for creating a database session to connect to.
    """
    async with get_async_session(url=str(settings.db.dsn_async), verbose=settings.db.verbose) as dbSession:
        yield dbSession


@pytest_asyncio.fixture(scope="module")
async def random_user(db: AsyncSession, mock_rgw_admin: MockRGWAdmin, mock_opa_service: MockOpa) -> AsyncIterator[User]:
    """
    Create a random user and deletes him afterward.
    """
    user = await create_random_user(db)
    mock_rgw_admin.create_user(uid=str(user.uid), display_name=user.display_name)
    mock_opa_service.add_user(user.lifescience_id)
    yield user
    mock_rgw_admin.delete_user(uid=str(user.uid))
    mock_opa_service.delete_user(user.lifescience_id)
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="module")
async def random_second_user(
    db: AsyncSession, mock_rgw_admin: MockRGWAdmin, mock_opa_service: MockOpa
) -> AsyncIterator[User]:
    """
    Create a random second user and deletes him afterward.
    """
    user = await create_random_user(db)
    mock_rgw_admin.create_user(uid=str(user.uid), display_name=user.display_name)
    mock_opa_service.add_user(user.lifescience_id)
    yield user
    mock_rgw_admin.delete_user(uid=str(user.uid))
    mock_opa_service.delete_user(user.lifescience_id)
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="module")
async def random_third_user(
    db: AsyncSession, mock_rgw_admin: MockRGWAdmin, mock_opa_service: MockOpa
) -> AsyncIterator[User]:
    """
    Create a random third user and deletes him afterwards.
    """
    user = await create_random_user(db)
    mock_rgw_admin.create_user(uid=str(user.uid), display_name=user.display_name)
    mock_opa_service.add_user(user.lifescience_id)
    yield user
    mock_rgw_admin.delete_user(uid=str(user.uid))
    mock_opa_service.delete_user(user.lifescience_id)
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="module")
async def random_bucket(
    db: AsyncSession, random_user: User, mock_s3_service: MockS3ServiceResource
) -> AsyncIterator[Bucket]:
    """
    Create a random bucket and deletes him afterwards.
    """
    bucket = Bucket(name=random_lower_string(), description=random_lower_string(), owner_id_bytes=random_user.uid.bytes)
    db.add(bucket)
    await db.commit()
    mock_s3_service.Bucket(bucket.name).create()
    yield bucket
    mock_s3_service.delete_bucket(bucket.name)
    await db.delete(bucket)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def cleanup(db: AsyncSession) -> AsyncIterator[CleanupList]:
    """
    Yields a Cleanup object where (async) functions can be registered which get executed after a (failed) test
    """
    cleanup_list = CleanupList()
    yield cleanup_list
    await cleanup_list.empty_queue()
