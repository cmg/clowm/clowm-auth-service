import random
import uuid

import pytest
from clowmdb.models import User
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter

from app.core.authorization import RoleEnum
from app.schemas.user import User as UserSchema
from app.tests.utils.user import get_authorization_headers


class _TestUserRoutes:
    base_path = "/users"


UserList = TypeAdapter(list[UserSchema])


class TestUserRoutesGet(_TestUserRoutes):
    @pytest.mark.asyncio
    async def test_get_user_me(self, client: AsyncClient, random_user: User) -> None:
        """
        Test for getting the currently logged-in user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowmdb.models.User
            Random user for testing.
        """
        headers = get_authorization_headers(random_user.uid)
        response = await client.get(f"{self.base_path}/me", headers=headers)
        assert response.status_code == status.HTTP_200_OK
        current_user = UserSchema.model_validate_json(response.content)
        assert current_user.uid == random_user.uid
        assert current_user.display_name == random_user.display_name
        assert current_user.roles is not None

    @pytest.mark.asyncio
    async def test_get_unknown_user(self, client: AsyncClient, user_token_headers: dict[str, str]) -> None:
        """
        Test for getting an unknown user by its uid.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        user_token_headers : dict[str,str]
            HTTP Headers to authorize the request.
        """
        response = await client.get(f"{self.base_path}/{uuid.uuid4()}", headers=user_token_headers)
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_user_by_uid(self, client: AsyncClient, random_user: User) -> None:
        """
        Test for getting a known user by its uid.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowmdb.models.User
            Random user for testing.
        """
        headers = get_authorization_headers(random_user.uid)
        response = await client.get(
            f"{self.base_path}/{str(random_user.uid)}", headers=headers, params={"include_roles": False}
        )
        assert response.status_code == status.HTTP_200_OK
        current_user = UserSchema.model_validate_json(response.content)
        assert current_user.uid == random_user.uid
        assert current_user.display_name == random_user.display_name
        assert current_user.roles is None

    @pytest.mark.asyncio
    async def test_get_user_by_uid_with_roles(self, client: AsyncClient, random_user: User) -> None:
        """
        Test for getting a known user by its uid.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowmdb.models.User
            Random user for testing.
        """
        headers = get_authorization_headers(random_user.uid)
        response = await client.get(
            f"{self.base_path}/{str(random_user.uid)}", headers=headers, params={"include_roles": True}
        )
        assert response.status_code == status.HTTP_200_OK
        current_user = UserSchema.model_validate_json(response.content)
        assert current_user.uid == random_user.uid
        assert current_user.display_name == random_user.display_name
        assert current_user.roles is not None

    @pytest.mark.asyncio
    async def test_search_user_by_name_substring(
        self, client: AsyncClient, random_user: User, user_token_headers: dict[str, str]
    ) -> None:
        """
        Test for searching a user by its name

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowmdb.models.User
            Random user for testing.
        user_token_headers : dict[str,str]
            HTTP Headers to authorize the request.
        """
        substring_indices = [0, 0]
        while substring_indices[1] - substring_indices[0] < 3:
            substring_indices = sorted(random.choices(range(len(random_user.display_name)), k=2))

        random_substring = random_user.display_name[substring_indices[0] : substring_indices[1]]

        response = await client.get(
            f"{self.base_path}", params={"name_like": random_substring}, headers=user_token_headers
        )
        assert response.status_code == status.HTTP_200_OK
        users = UserList.validate_json(response.content)

        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1

    @pytest.mark.asyncio
    async def test_list_all_user(
        self, client: AsyncClient, random_user: User, user_token_headers: dict[str, str]
    ) -> None:
        """
        Test for searching a user by its name

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowmdb.models.User
            Random user for testing..
        user_token_headers : dict[str,str]
            HTTP Headers to authorize the request.
        """

        response = await client.get(f"{self.base_path}", headers=user_token_headers)
        assert response.status_code == status.HTTP_200_OK

        users = UserList.validate_json(response.content)
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1
        for u in users:
            assert u.roles is None

    @pytest.mark.asyncio
    async def test_filter_user_by_role(
        self, client: AsyncClient, random_user: User, user_token_headers: dict[str, str]
    ) -> None:
        """
        Test for list and filter users by their role.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowmdb.models.User
            Random user for testing.
        user_token_headers : dict[str,str]
            HTTP Headers to authorize the request.
        """

        response = await client.get(
            f"{self.base_path}",
            params={"filter_roles": RoleEnum.USER.value, "include_roles": True},
            headers=user_token_headers,
        )
        assert response.status_code == status.HTTP_200_OK

        users = UserList.validate_json(response.content)
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1
        for u in users:
            assert u.roles is not None
            assert RoleEnum.USER.value in u.roles

    @pytest.mark.asyncio
    async def test_list_users_with_roles(
        self, client: AsyncClient, random_user: User, user_token_headers: dict[str, str]
    ) -> None:
        """
        Test for searching a user by its name

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowmdb.models.User
            Random user for testing.
        user_token_headers : dict[str,str]
            HTTP Headers to authorize the request.
        """

        response = await client.get(f"{self.base_path}", params={"include_roles": True}, headers=user_token_headers)
        assert response.status_code == status.HTTP_200_OK

        users = UserList.validate_json(response.content)
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1
        for u in users:
            assert u.roles is not None
