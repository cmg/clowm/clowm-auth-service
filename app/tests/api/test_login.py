import urllib.parse
from uuid import UUID

import pytest
from clowmdb.models import Bucket, User
from fastapi import status
from httpx import AsyncClient
from sqlalchemy import delete, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.security import decode_token
from app.tests.mocks.mock_rgw_admin import MockRGWAdmin
from app.tests.mocks.mock_s3_resource import MockS3ServiceResource
from app.tests.utils.utils import CleanupList, random_lower_string


class TestLoginRoute:
    login_path: str = "/auth/"

    @pytest.mark.asyncio
    async def test_login_redirect(self, client: AsyncClient) -> None:
        """
        Test for the query parameter on the login redirect route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        r = await client.get(
            self.login_path + "login",
            params={"return_path": "/dashboard", "provider": "lifescience"},
            follow_redirects=False,
        )
        assert r.status_code == status.HTTP_302_FOUND

    @pytest.mark.asyncio
    async def test_successful_login_with_existing_user(self, client: AsyncClient, random_user: User) -> None:
        """
        Test for login callback route with an existing user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowmdb.models.User
            Random user for testing.
        """

        login_response = await client.get(
            self.login_path + "login", params={"return_path": "/dashboard"}, follow_redirects=False
        )

        r = await client.get(
            self.login_path + "callback/lifescience",
            params={
                "sub": random_user.lifescience_id,
                "name": random_user.display_name,
                "email": random_user.email,
            },
            follow_redirects=False,
            cookies=login_response.cookies,
        )
        assert r.status_code == status.HTTP_302_FOUND
        assert "set-cookie" in r.headers.keys()
        cookie_header = r.headers["set-cookie"]
        right_header = None
        for t in cookie_header.split(";"):
            if t.startswith("bearer"):
                right_header = t
                break
        assert right_header
        claim = decode_token(right_header.split("=")[1])
        assert claim["sub"] == str(random_user.uid)
        assert r.headers["location"].startswith(f"/?return_path={urllib.parse.quote_plus('/dashboard')}")

    @pytest.mark.asyncio
    async def test_successful_login_with_existing_user_and_different_email(
        self,
        client: AsyncClient,
        random_user: User,
        db: AsyncSession,
    ) -> None:
        """
        Test for login callback route with an existing user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowmdb.models.User
            Random user for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        new_mail = f"{random_lower_string(10)}@example.com"
        r = await client.get(
            self.login_path + "callback/lifescience",
            params={
                "sub": random_user.lifescience_id,
                "name": random_user.display_name,
                "email": new_mail,
            },
            follow_redirects=False,
        )
        assert r.status_code == status.HTTP_302_FOUND
        assert "set-cookie" in r.headers.keys()
        cookie_header = r.headers["set-cookie"]
        right_header = None
        for t in cookie_header.split(";"):
            if t.startswith("bearer"):
                right_header = t
                break
        assert right_header
        claim = decode_token(right_header.split("=")[1])
        assert claim["sub"] == str(random_user.uid)
        assert r.headers["location"] == "/"

        db_user = await db.scalar(select(User).where(User.uid_bytes == random_user.uid.bytes))
        assert db_user
        assert db_user.email == new_mail

    @pytest.mark.asyncio
    async def test_login_with_error(self, client: AsyncClient) -> None:
        """
        Test for login callback route with an existing user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        r = await client.get(
            self.login_path + "callback/lifescience",
            params={"sub": "", "name": "", "email": "", "error": True},
            follow_redirects=False,
        )
        assert r.status_code == status.HTTP_302_FOUND
        if "set-cookie" in r.headers.keys():
            assert find_cookie(searched_cookie_name="bearer", cookie_header=r.headers["set-cookie"]) is None
        assert "login_error=" in r.headers["location"]

    @pytest.mark.asyncio
    async def test_successful_login_with_non_existing_user(
        self,
        client: AsyncClient,
        mock_rgw_admin: MockRGWAdmin,
        db: AsyncSession,
        mock_s3_service: MockS3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for login callback route with a non-existing user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        mock_rgw_admin : app.tests.mocks.mock_rgw_admin.MockRGWAdmin
            Mock RGW admin for Ceph.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service Resource for testing.
        """
        lifescience_id = random_lower_string()
        display_name = f"{random_lower_string(8)} {random_lower_string(8)}"
        r = await client.get(
            self.login_path + "callback/lifescience",
            params={
                "sub": lifescience_id,
                "name": display_name,
                "email": f"{random_lower_string(10)}@example.com",
            },
            follow_redirects=False,
        )
        # Check response and valid/right jwt token
        assert r.status_code == status.HTTP_302_FOUND
        assert "login_error" not in r.headers["location"]
        assert "set-cookie" in r.headers.keys()
        cookie = find_cookie(searched_cookie_name="bearer", cookie_header=r.headers["set-cookie"])
        assert cookie is not None
        claim = decode_token(cookie.split("=")[1])
        assert claim.get("sub", None) is not None
        uid = UUID(claim["sub"])

        async def cleanup_db() -> None:
            await db.execute(delete(Bucket).where(Bucket.owner_id_bytes == uid.bytes))
            await db.execute(delete(User).where(User.uid_bytes == uid.bytes))
            await db.commit()

        cleanup.add_task(cleanup_db)
        cleanup.add_task(mock_rgw_admin.delete_user, str(uid))

        # Check that user is created in RGW
        assert mock_rgw_admin.get_user(claim["sub"])["keys"][0]["user"] is not None

        # Check that user is created in DB
        db_user = await db.scalar(select(User).where(User.uid_bytes == uid.bytes))
        assert db_user
        assert db_user.lifescience_id == lifescience_id
        assert db_user.display_name == display_name
        assert db_user.initialized

        # Check that upload and download bucket are created
        db_buckets = (
            (await db.execute(select(Bucket).where(Bucket.owner_id_bytes == db_user.uid.bytes))).scalars().all()
        )
        assert len(db_buckets) == 1
        # Check that buckets are created in S3
        for bucket in db_buckets:
            cleanup.add_task(mock_s3_service.delete_bucket, db_buckets[0].name)
            assert mock_s3_service.check_bucket_exists(bucket.name)
            # Check that bucket policy is set
            assert mock_s3_service.BucketPolicy(bucket.name).policy
            # Check that cors rule is set
            assert mock_s3_service.Bucket(bucket.name).Cors().rules is not None

            assert bucket.size_limit is not None
            assert bucket.object_limit is not None


def find_cookie(searched_cookie_name: str, cookie_header: str) -> str | None:
    """
    Find a specific cookie in the set-cookie header of a HTTP response

    Parameters
    ----------
    searched_cookie_name : str
        Name of the cookie to be searched
    cookie_header : str
        Cookie string from HTTP header

    Returns
    -------
    cookie : str | None
        Returns the cookie if it is present, None otherwise
    """
    for cookie in cookie_header.split(";"):
        if cookie.startswith(searched_cookie_name):
            return cookie
    return None
