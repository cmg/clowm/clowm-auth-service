import pytest
from clowmdb.models import User
from fastapi import status
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from app.schemas.security import ErrorDetail


class TestJWTProtectedRoutes:
    protected_route: str = "/users/me"

    @pytest.mark.asyncio
    async def test_missing_authorization_header(self, client: AsyncClient) -> None:
        """
        Test with missing authorization header on a protected route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response = await client.get(self.protected_route)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        error = ErrorDetail.model_validate_json(response.content)
        assert error.detail == "Not authenticated"

    @pytest.mark.asyncio
    async def test_malformed_authorization_header(self, client: AsyncClient) -> None:
        """
        Test with malformed authorization header on a protected route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response = await client.get(self.protected_route, headers={"Authorization": "Bearer not-a-jwt-token"})
        assert response.status_code == status.HTTP_400_BAD_REQUEST

        error = ErrorDetail.model_validate_json(response.content)
        assert error.detail == "Malformed JWT"

    @pytest.mark.asyncio
    async def test_correct_authorization_header(self, client: AsyncClient, user_token_headers: dict[str, str]) -> None:
        """
        Test with correct authorization header on a protected route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        user_token_headers : dict[str,str]
            HTTP Headers to authorize the request.
        """
        response = await client.get(self.protected_route, headers=user_token_headers)
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_protected_route_with_deleted_user(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: User,
        user_token_headers: dict[str, str],
    ) -> None:
        """
        Test with correct authorization header from a deleted user on a protected route.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowmdb.models.User
            Random user for testing. pytest fixture.
        user_token_headers : dict[str,str]
            HTTP Headers to authorize the request.
        """
        await db.delete(random_user)
        await db.commit()

        response = await client.get(self.protected_route, headers=user_token_headers)
        assert response.status_code == status.HTTP_404_NOT_FOUND
