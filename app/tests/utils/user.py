from uuid import UUID

import pytest
from clowmdb.models import User
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.authorization import RoleEnum
from app.core.security import create_access_token

from .utils import random_lower_string


@pytest.mark.asyncio
def get_authorization_headers(uid: UUID) -> dict[str, str]:
    """
    Login a user and return the correct headers for subsequent requests.

    Parameters
    ----------
    uid : str
        UID of the user who should be logged in.

    Returns
    -------
    headers : dict[str,str]
        HTTP Headers to authorize each request.
    """
    jwt = create_access_token(str(uid), roles=[RoleEnum.USER])
    headers = {"Authorization": f"Bearer {jwt}"}
    return headers


@pytest.mark.asyncio
async def create_random_user(db: AsyncSession) -> User:
    """
    Creates a random user in the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.

    Returns
    -------
    user : clowmdb.models.User
        Newly created user.
    """
    user = User(
        lifescience_id=random_lower_string(),
        display_name=random_lower_string(),
        email=f"{random_lower_string(8)}@example.de",
    )
    db.add(user)
    await db.commit()
    return user
