import random
import string
from datetime import datetime
from inspect import iscoroutinefunction
from typing import Any, Awaitable, Callable, Generic, ParamSpec, TypeVar

P = ParamSpec("P")
T = TypeVar("T")


class Job(Generic[P, T]):
    def __init__(self, func: Callable[P, T], *args: P.args, **kwargs: P.kwargs) -> None:
        self.func = func
        self.args = args
        self.kwargs = kwargs

    @property
    def is_async(self) -> bool:
        return iscoroutinefunction(self.func)

    def __call__(self) -> T:
        return self.func(*self.args, **self.kwargs)


class AsyncJob(Job):
    def __init__(self, func: Callable[P, Awaitable[T]], *args: P.args, **kwargs: P.kwargs) -> None:
        super().__init__(func, *args, **kwargs)
        assert iscoroutinefunction(self.func)

    async def __call__(self) -> T:
        return await super().__call__()


class CleanupList:
    """
    Helper object to hold a queue of functions that can be executed later
    """

    def __init__(self) -> None:
        self.queue: list[Job] = []

    def add_task(self, func: Callable[P, Any], *args: P.args, **kwargs: P.kwargs) -> None:
        """
        Add a (async) function to the queue.

        Parameters
        ----------
        func : Callable[P, Any]
            Function to register.
        args : P.args
            Arguments to the function.
        kwargs : P.kwargs
            Keyword arguments to the function.
        """
        if iscoroutinefunction(func):
            self.queue.append(AsyncJob(func, *args, **kwargs))
        else:
            self.queue.append(Job(func, *args, **kwargs))

    async def empty_queue(self) -> None:
        """
        Empty the queue by executing the registered functions.
        """
        while len(self.queue) > 0:
            func = self.queue.pop()
            if func.is_async:
                await func()
            else:
                func()


def random_lower_string(length: int = 32) -> str:
    """
    Creates a random string with arbitrary length.

    Parameters
    ----------
    length : int, default 32
        Length for the random string.

    Returns
    -------
    string : str
        Random string.
    """
    return "".join(random.choices(string.ascii_lowercase, k=length))


def random_ipv4_string() -> str:
    """
    Creates a random IPv4 address.

    Returns
    -------
    string : str
        Random IPv4 address.
    """
    return ".".join(str(random.randint(0, 255)) for _ in range(4))


def json_datetime_converter(obj: Any) -> str | None:
    """
    helper function for the json converter to covert the object into a string format if it is a datetime object.\n
    Parse a datetime object into the format YYYY-MM-DDTHH:MM:SS, e.g. 2022-01-01T00:00:00

    Parameters
    ----------
    obj : Any
        Object to try convert as a datetime object.

    Returns
    -------
    time : str | None
        The str representation of a datetime object, None otherwise
    """
    if isinstance(obj, datetime):
        return obj.strftime("%Y-%m-%dT%H:%M:%S")
    return None
