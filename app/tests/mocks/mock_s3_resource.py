from datetime import datetime
from typing import TYPE_CHECKING

CORSConfig = list[dict[str, list[str] | int]]

if TYPE_CHECKING:
    from mypy_boto3_s3.type_defs import CORSConfigurationTypeDef
else:
    CORSConfigurationTypeDef = object


class MockS3BucketPolicy:
    """
    Mock S3 bucket policy for the boto3 BucketPolicy for testing purposes.

    Functions
    ---------
    put(Policy: str) -> None
        Save a new bucket policy.

    Attributes
    ----------
    bucket_name : str
        Name of the corresponding bucket.
    policy : str
        The policy in as a string.
    """

    def __init__(self, bucket_name: str):
        self.bucket_name = bucket_name
        self.policy: str = ""

    def put(self, Policy: str) -> None:
        """
        Save a new bucket policy.

        Parameters
        ----------
        Policy : str
            The new policy as str.
        """
        self.policy = Policy


class MockS3CorsRule:
    """
    Mock S3 Cors Configuration for the boto3 BucketCors for testing purposes.

    Functions
    ---------
    put(CORSConfiguration: CORSConfig) -> None
        Save a new bucket CORS rule.

    Attributes
    ----------
    rules : str
        List of all CORS rules on the bucket.
    """

    def __init__(self) -> None:
        self.rules: CORSConfigurationTypeDef | None = None

    def put(self, CORSConfiguration: CORSConfigurationTypeDef) -> None:
        """
        Save a new bucket CORS rule.

        Parameters
        ----------
        CORSConfiguration : mypy_boto3_s3.type_defs.CORSConfigurationTypeDef
            The new policy as str.

        Notes
        -----
        A configuration has the following form
        {
            "CORSRules": [
                {
                    "ID": string,
                    "AllowedHeaders": list[string],
                    "AllowedMethods": list[string],
                    "AllowedOrigins": list[string],
                    "ExposeHeaders": list[string],
                    "MaxAgeSeconds": int,
                },
            ]
        }
        """
        self.rules = CORSConfiguration


class MockS3Bucket:
    """
    Mock S3 bucket for the boto3 Bucket for testing purposes.

    Functions
    ---------
    Policy() -> app.tests.mocks.mock_s3_resource.MockS3BucketPolicy
        Get the bucket policy from the bucket.
    create() -> None
        Create the bucket in the mock service.

    Attributes
    ----------
    name: str
        name of the bucket.
    creation_date : datetime
        Creation date of the bucket.
    """

    def __init__(self, name: str, parent_service: "MockS3ServiceResource"):
        """
        Initialize a MockS3Bucket.

        Parameters
        ----------
        name : str
            Name of the bucket.
        parent_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock service object where the bucket will be saved.
        """
        self.name = name
        self.creation_date: datetime = datetime.now()
        self._parent_service: MockS3ServiceResource = parent_service
        self.policy = MockS3BucketPolicy(name)
        self.cors = MockS3CorsRule()

    def Policy(self) -> MockS3BucketPolicy:
        """
        Get the bucket policy from the bucket.

        Returns
        -------
        bucket_policy : app.tests.mocks.mock_s3_resource.MockS3BucketPolicy
            The corresponding bucket policy.
        """
        return self.policy

    def Cors(self) -> MockS3CorsRule:
        return self.cors

    def create(self, ObjectOwnership: str | None = None) -> None:
        """
        Create the bucket in the mock S3 service.
        """
        self._parent_service.create_bucket(self)

    def __repr__(self) -> str:
        return f"MockS3Bucket(name={self.name})"


class MockS3ServiceResource:
    """
    A functional mock class of the boto3 S3ServiceResource for testing purposes.

    Functions
    ---------
    Bucket(name: str) -> app.tests.mocks.mock_s3_resource.MockS3bucket
        Get/Create a mock bucket.
    BucketPolicy(bucket_name: str) -> app.tests.mocks.mock_s3_resource.MockS3BucketPolicy
        Get the bucket policy from the corresponding bucket.
    create_bucket(bucket: app.tests.mocks.mock_s3_resource.MockS3bucket) -> None
        Create a bucket in the mock service.
    """

    def __init__(self) -> None:
        self._buckets: dict[str, MockS3Bucket] = {}

    def Bucket(self, name: str) -> MockS3Bucket:
        """
        Get an existing bucket from the mock service or creat a new mock bucket but doesn't save it yet.
        Call `bucket.create()` on returned object for that.

        Parameters
        ----------
        name : str
            Name of the bucket.

        Returns
        -------
        bucket : app.tests.mocks.mock_s3_resource.MockS3bucket
            Existing/Created mock bucket.
        """
        return self._buckets[name] if name in self._buckets else MockS3Bucket(name=name, parent_service=self)

    def BucketPolicy(self, bucket_name: str) -> MockS3BucketPolicy:
        """
        Get the bucket policy from the corresponding bucket.

        Parameters
        ----------
        bucket_name : str
            Name of the bucket.

        Returns
        -------
        bucket_policy : app.tests.mocks.mock_s3_resource.MockS3BucketPolicy
            The corresponding bucket policy.
        """
        return self._buckets[bucket_name].policy

    def create_bucket(self, bucket: MockS3Bucket) -> None:
        """
        Create a bucket in the mock service.
        Convenience function for testing.

        Parameters
        ----------
        bucket : app.tests.mocks.mock_s3_resource.MockS3bucket
            Bucket which should be created.
        """
        self._buckets[bucket.name] = bucket

    def delete_bucket(self, name: str) -> None:
        """
        Delete am empty bucket in the mock service.
        Convenience function for testing.

        Parameters
        ----------
        name : str
            Name of the bucket.
        """
        if name in self._buckets:
            del self._buckets[name]

    def check_bucket_exists(self, name: str) -> bool:
        """
        Check if an bucket with a given name exists

        Parameters
        ----------
        name : str
            Name of a bucket

        Returns
        -------
        exists : bool
            Returns flag if the bucket exists
        """
        return name in self._buckets.keys()

    def __repr__(self) -> str:
        return f"MockS3ServiceResource(buckets={[bucket_name for bucket_name in self._buckets.keys()]})"
