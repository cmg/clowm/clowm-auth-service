from uuid import uuid4

import httpx
from fastapi import status

from app.core.security import RoleEnum
from app.schemas.security import AuthzResponse, OPARequest, RolesRequest, RolesResponse


class MockOpa:
    def __init__(self) -> None:
        self.users: dict[str, list[RoleEnum]] = {}

    def handle_request(self, request: httpx.Request) -> httpx.Response:
        """
        Handle a mock request to OPA.

        Parameters
        ----------
        request : httpx.Request
            Raw request to mock OPA.

        Returns
        -------
        response : httpx.Response
            Raw response from mock OPA.
        """
        # Parse Request
        opa_request = OPARequest.model_validate_json(request.content)
        if request.url.path == "/v1/data/clowm/authz/allow":  # if request is for authorization
            # Always return successful authorization request
            return httpx.Response(
                status_code=status.HTTP_200_OK, text=AuthzResponse(result=True, decision_id=uuid4()).model_dump_json()
            )
        elif request.url.path == "/v1/data/clowm/roles/roles":  # if request is for roles
            parsed_request = RolesRequest(**opa_request.input)
            response_body = RolesResponse(
                decision_id=uuid4(),
                result={uid: self.users[uid] for uid in parsed_request.uids if uid in self.users.keys()},
            )
            return httpx.Response(status_code=status.HTTP_200_OK, text=response_body.model_dump_json())
        return httpx.Response(status_code=status.HTTP_400_BAD_REQUEST)

    def add_user(self, uid: str, roles: list[RoleEnum] | None = None) -> None:
        """
        Add a user to the Mock LDAP.

        Parameters
        ----------
        uid : str
            UID of the user to add.
        roles : list[app.core.authorization.RoleEnum] | None, default None
            Optional roles of the new user. If None, the user gets the standard Role RoleEnum.USER
        """
        self.users[uid] = [RoleEnum.USER] if roles is None else roles

    def delete_user(self, uid: str) -> None:
        """
        Delete a user in the Mock LDAP.

        Parameters
        ----------
        uid : str
            UID of the user to delete.
        """
        if uid in self.users.keys():
            del self.users[uid]
