from app.tests.utils.utils import random_lower_string


class MockRGWAdmin:
    """
    A functional mock class of the rgwadmin.RGWAdmin for testing purposes.

    Functions
    ---------
    get_user(uid: str, stats: bool = False) -> dict[str, list[dict[str, str]]]
        Returns a dict with only one key 'keys'.
    create_key(uid: str, key_type: str = "s3", generate_key: bool = True) -> dict[str, list[dict[str, str]]]
        Creates a new key for a user.
    remove_key(access_key: str, uid: str) -> None
        Remove a key for a user.
    delete_user(uid: str) -> None
        Deletes all keys for a user.
    """

    _keys: dict[str, list[dict[str, str]]]

    def __init__(self) -> None:
        self._keys = {}

    def set_bucket_quota(
        self,
        uid: str,
        bucket: str,
        max_objects: int,
        max_size_kb: int,
        enabled: bool,
    ) -> None:
        pass

    def create_user(self, uid: str, display_name: str, max_buckets: int = -1) -> None:
        new_key = {"user": uid, "access_key": random_lower_string(20).upper(), "secret_key": random_lower_string(40)}
        if uid not in self._keys:
            self._keys[uid] = [new_key]

    def get_user(self, uid: str, stats: bool = False) -> dict[str, list[dict[str, str]]]:  # noqa
        """
        Get the keys from a user.

        Parameters
        ----------
        uid : str
            Username of a user.
        stats : bool
            Include stats in response. Will be ignored.

        Returns
        -------
        user_keys : dict[str, list[dict[str, str]]]
            The user object with the associated keys. See Notes.

        Notes
        -----
        The dict this function returns has this form:

        { "keys" : [S3Key] }

        where each S3 Key dict has this form<br>

        {
            "user" : str,
            "access_key" : str,
            "secret_key" : str
        }
        """
        if uid in self._keys:
            return {"keys": self._keys[uid]}
        return {"keys": []}

    def delete_user(self, uid: str) -> None:
        """
        Remove all S3 keys for a user.
        Convenience function for testing.

        Parameters
        ----------
        uid : str
            Username of a user.
        """
        self._keys[uid] = []
