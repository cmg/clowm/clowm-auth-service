import pytest
from clowmdb.models import Bucket, User
from sqlalchemy import delete, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud import CRUDBucket
from app.tests.utils.utils import CleanupList, random_lower_string


class TestUserCRUD:
    @pytest.mark.asyncio
    async def test_create_bucket(self, db: AsyncSession, random_user: User, cleanup: CleanupList) -> None:
        """
        Test for creating a bucket in the Bucket CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowmdb.models.User
            Random user for testing.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        bucket = Bucket(
            name=random_lower_string(), description=random_lower_string(), owner_id_bytes=random_user.uid.bytes
        )
        created_bucket = await CRUDBucket.create(bucket, db=db)
        assert created_bucket is not None

        async def delete_bucket() -> None:
            await db.execute(delete(Bucket).where(Bucket.name == created_bucket.name))
            await db.commit()

        cleanup.add_task(delete_bucket)

        db_bucket = await db.scalar(select(Bucket).where(Bucket.name == bucket.name))
        assert db_bucket
        assert db_bucket == bucket

    @pytest.mark.asyncio
    async def test_get_bucket_by_name(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for getting a bucket by name from the Bucket CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowmdb.models.Bucket
            Random bucket for testing.
        """
        bucket = await CRUDBucket.get(random_bucket.name, db=db)
        assert bucket
        assert bucket == random_bucket

    @pytest.mark.asyncio
    async def test_get_unknown_bucket_by_name(
        self,
        db: AsyncSession,
    ) -> None:
        """
        Test for getting an unknown bucket by name from the Bucket CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        bucket = await CRUDBucket.get(random_lower_string(length=16), db=db)
        assert bucket is None
