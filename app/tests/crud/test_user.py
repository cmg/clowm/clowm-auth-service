import random
import uuid

import pytest
from clowmdb.models import User
from sqlalchemy import delete, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud import CRUDUser
from app.tests.utils.utils import CleanupList, random_lower_string


class TestUserCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_user(self, db: AsyncSession, cleanup: CleanupList) -> None:
        """
        Test for creating a user in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        user = User(lifescience_id=random_lower_string(), display_name=random_lower_string())
        created_user = await CRUDUser.create(user, db=db)
        assert created_user is not None

        async def delete_user() -> None:
            await db.execute(delete(User).where(User.uid_bytes == created_user.uid.bytes))
            await db.commit()

        cleanup.add_task(delete_user)

        db_user = await db.scalar(select(User).where(User.uid_bytes == user.uid.bytes))
        assert db_user
        assert db_user.uid == user.uid


class TestUserCRUDUpdate:
    @pytest.mark.asyncio
    async def test_update_user_email(self, db: AsyncSession, random_user: User) -> None:
        """
        Test for updating a user email in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowmdb.models.User
            Random user for testing.
        """
        new_mail = f"{random_lower_string(9)}@example.com"
        await CRUDUser.update_email(random_user.uid, new_mail, db=db)

        db_user = await db.scalar(select(User).where(User.uid_bytes == random_user.uid.bytes))
        assert db_user
        assert db_user.uid == random_user.uid
        assert db_user.email == new_mail

    @pytest.mark.asyncio
    async def test_mark_user_initialized(self, db: AsyncSession, random_user: User) -> None:
        """
        Test for marking a user initialized in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowmdb.models.User
            Random user for testing.
        """
        await CRUDUser.mark_initialized(random_user.uid, db=db)

        db_user = await db.scalar(select(User).where(User.uid_bytes == random_user.uid.bytes))
        assert db_user
        assert db_user.uid == random_user.uid
        assert db_user.initialized


class TestUserCRUDGet:
    @pytest.mark.asyncio
    async def test_get_user_by_id(self, db: AsyncSession, random_user: User) -> None:
        """
        Test for getting a user by id from the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowmdb.models.User
            Random user for testing.
        """
        user = await CRUDUser.get(random_user.uid, db=db)
        assert user
        assert random_user.uid == user.uid
        assert random_user.display_name == user.display_name

    @pytest.mark.asyncio
    async def test_get_user_by_lifescience_id(self, db: AsyncSession, random_user: User) -> None:
        """
        Test for getting a user by lifescience id from the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowmdb.models.User
            Random user for testing.
        """
        user = await CRUDUser.get_by_lifescience_id(random_user.lifescience_id, db=db)
        assert user
        assert random_user.uid == user.uid
        assert random_user.display_name == user.display_name

    @pytest.mark.asyncio
    async def test_get_unknown_user_by_id(
        self,
        db: AsyncSession,
    ) -> None:
        """
        Test for getting an unknown user by id from the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        user = await CRUDUser.get(uuid.uuid4(), db=db)
        assert user is None

    @pytest.mark.asyncio
    async def test_search_successful_user_by_name(self, db: AsyncSession, random_user: User) -> None:
        """
        Test for searching a user by a substring of his name in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowmdb.models.User
            Random user for testing.
        """
        substring_indices = [0, 0]
        while substring_indices[0] == substring_indices[1]:
            substring_indices = sorted(random.choices(range(len(random_user.display_name)), k=2))

        random_substring = random_user.display_name[substring_indices[0] : substring_indices[1]]
        users = await CRUDUser.list_users(name_substring=random_substring, db=db)
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1

    @pytest.mark.asyncio
    async def test_search_non_existing_user_by_name(self, db: AsyncSession, random_user: User) -> None:
        """
        Test for searching a non-existing user by a substring of his name in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowmdb.models.User
            Random user for testing.
        """
        users = await CRUDUser.list_users(name_substring=2 * random_user.display_name, db=db)
        assert sum(1 for u in users if u.uid == random_user.uid) == 0


class TestUserCRUDList:
    @pytest.mark.asyncio
    async def test_list_all_users(self, db: AsyncSession, random_user: User) -> None:
        """
        Test for listing all the users in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowmdb.models.User
            Random user for testing.
        """
        users = await CRUDUser.list_users(db=db)
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1
