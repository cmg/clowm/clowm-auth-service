## Development Setup

### Python Setup 🐍
Currently, only Python version `>=3.10` is supported because it uses its new features for type annotations
to write more compact code. Since FastAPI relies on these type annotations and `mypy` is integrated into
this project, we make heavy use of this feature.

Write
```python
var1: list[int] = [1,2,3]
var2: str | None = None
```
instead of
```python
from typing import List, Optional

var1: List[int] = [1,2,3]
var2: Optional[str] = None
```
### Environment Setup
Create a virtual environment, install the dependencies and install the [pre-commit](https://pre-commit.com/) hooks.<br>
The linters can prevent a commit of the code quality doesn't meet the standard.
```shell
python -m venv venv
source venv/bin/activate
python -m pip install -r requirements.txt
python -m pip install -r requirements-dev.txt
pre-commit install
```
Look at [CloWM-Database](https://gitlab.ub.uni-bielefeld.de/cmg/clowm/clowm-database) on how to install the `clowmdb`
package for developing purposes.

### Ceph Setup
Setup up or connect to an ceph cluster.

A user with `user` capabilities should be created, e.g.<br>
`radosgw-admin user create --uid=myadmin --caps="users=*"`

### Database Setup
#### Dev database
The easiest solution is [Docker](https://docs.docker.com/get-docker/) with an attached volume
to set up a MySQL database.
```shell
docker volume create clowmdb_dev
docker run --name clowm_dev_db \
  -e MYSQL_RANDOM_ROOT_PASSWORD=yes \
  -e MYSQL_DATABASE=<database_name> \
  -e MYSQL_USER=<database_user> \
  -e MYSQL_PASSWORD=<database_password> \
  -p 127.0.0.1:3306:3306 \
  -v clowmdb_dev:/var/lib/mysql \
  -d \
  mysql:8
```
When the container stopped just restart it with
```shell
docker start clowm_dev_db
```
Look at the [Environment Variables](README.md#environment-variables) section to see which env variables have to be set.
#### Test database
Set up a second database on a different port for the integration tests. This database doesn't have to be persistent
because all data will be purged after each test run.
```shell
docker run --name clowm_test_db \
  -e MYSQL_RANDOM_ROOT_PASSWORD=yes \
  -e MYSQL_DATABASE=<database_name> \
  -e MYSQL_USER=<database_user> \
  -e MYSQL_PASSWORD=<database_password> \
  -p 127.0.0.1:8001:3306 \
  --rm \
  -d \
  mysql:8
```

#### Database Migrations
Use [CloWM Database](https://gitlab.ub.uni-bielefeld.de/cmg/clowm/clowm-database) for setting up the database.

### Dev OIDC Provider Setup
To avoid the complex process of connecting the local machine with the LifeScience AAI Test server, a simple [OIDC provider](https://github.com/Soluto/oidc-server-mock)
can be setup with Docker.

Copy the `oidc_dev_example` directory to `oidc_dev`
```shell
cp -r oidc_dev_example oidc_dev
```
In the file `oidc_dev/clients_config.json` add a random value to `ClientId` and `ClientSecrets`. These can be generated for example with `openssl`.
```shell
openssl rand -hex 10
```
You can add/delete users in the file `oidc_dev/users_config.json` according the schema that is provided there.<br>
Adjust the volume path and start the docker container
```shell
docker run --name clowm_oidc_provider \
  -e CLIENTS_CONFIGURATION_PATH=/tmp/config/clients_config.json \
  -e IDENTITY_RESOURCES_PATH=/tmp/config/identity_resources.json \
  -e USERS_CONFIGURATION_PATH=/tmp/config/users_config.json \
  -e SERVER_OPTIONS_PATH=/tmp/config/server_options.json \
  -e ASPNETCORE_ENVIRONMENT=Development \
  -p 127.0.0.1:8002:80 \
  -v /path/to/folder/oidc_dev:/tmp/config:ro \
  -d \
  ghcr.io/soluto/oidc-server-mock:latest
```
Set the env variables `OIDC_BASE_URI` to `http://localhost:8002` and `OIDC_CLIENT_SECRET` / `OIDC_CLIENT_ID` to their appropriate value.

### Reverse Proxy Setup
A reverse proxy should be used even for developing. Once set up it simplifies the communication between all
services drastically.

The reverse proxy should do the following things
 * It forwards all request to http://localhost:9999/api/* to http://localhost:8080 (this backend)
 * It strips the prefix `/api` before it forwards the request to the backend
 * All other request will be forwarded to http://localhost:5173, the corresponding dev [Frontend](https://gitlab.ub.uni-bielefeld.de/denbi/object-storage-access-ui)
 * Hides all the RADOS Gateways behind http://localhost:9998 and distributes all requests equally to the Gateways
 * Takes care of the CORS header for the RADOS Gateway

You can use any reverse proxy for this task, like
* [Traefik](https://traefik.io/traefik/)
* [Caddy](https://caddyserver.com/)
* [HAProxy](https://www.haproxy.org/)
* [nginx](https://nginx.org/en/)
* [Envoy Proxy](https://www.envoyproxy.io/)

### Run Dev Server
Export all necessary environment variables or create a `.env` file.<br>
Run the dev server with live reload after changes
```shell
set -e
python app/check_ceph_connection.py
python app/check_oidc_connection.py
python app/check_database_connection.py
uvicorn app.main:app --reload
```
You can check your code with linters or even automatically reformat files based on these rules
```shell
./scripts/lint.sh      # check code
./scripts/format.sh    # reformat code
```

### Run Tests
Export the port and other variables of the database and then start the test script
```shell
export DB_PORT=8001
# Clean database with clowmdb
pytest --cov=app --cov-report=html app/tests
```

### Common Problems
Q: When I start the server I get the error `ModuleNotFoundError: No module named 'app'`<br>
A: Export the `PYTHONPATH` variable with the current working directory
```shell
export PYTHONPATH=$(pwd)
```

Q: When I start the linters `isort`, `black`, etc. cannot be found<br>
A: Prepend every call with `python -m`
```shell
python -m isort
python -m black
...
```
