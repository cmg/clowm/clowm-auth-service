#!/bin/sh -e
set -x

ruff check --fix --show-fixes app
black app
isort app
