#! /usr/bin/env bash

set -e

# Check Connection to Ceph RGW
python app/check_ceph_connection.py
# Check Connection to OIDC provider
python app/check_oidc_connection.py
# Let the DB start
python app/check_database_connection.py
