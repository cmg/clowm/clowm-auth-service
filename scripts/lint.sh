#!/usr/bin/env bash

set -x

ruff --version
ruff check app

isort --version
isort -c app

black --version
black app --check

mypy --version
mypy app
