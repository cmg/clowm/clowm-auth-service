# 🚨 Deprecation 🚨
This repository was merged into the [CloWM backend](https://gitlab.ub.uni-bielefeld.de/cmg/clowm/clowm-backend) and will NOT be developed further.  
Everything is in read-only mode.

# CloWM Auth Service

## Description

This is the Auth service of the CloWM service.

## Configuration

### General

| Env variable                | Config file key       | Default       | Value    | Example                | Description                                                                                                           |
|-----------------------------|-----------------------|---------------|----------|------------------------|-----------------------------------------------------------------------------------------------------------------------|
| `CLOWM_CONFIG_FILE_YAML`    | -                     | `config.yaml` | Filepath | `/path/to/config.yaml` | Path to a YAML file to read the config. See [example-config/example-config.yaml](example-config/example-config.yaml). |
| `CLOWM_CONFIG_FILE_TOML`    | -                     | `config.toml` | Filepath | `/path/to/config.toml` | Path to a TOML file to read the config. See [example-config/example-config.toml](example-config/example-config.toml). |
| `CLOWM_CONFIG_FILE_JSON`    | -                     | `config.json` | Filepath | `/path/to/config.json` | Path to a JSON file to read the config. See [example-config/example-config.json](example-config/example-config.json). |
| `CLOWM_API_PREFIX`          | `api_prefix`          | unset         | URI path | `/api`                 | Prefix before every URL path                                                                                          |
| * `CLOWM_UI_URI`            | `ui_uri`              | unset         | HTTP URL | `https://localhost`    | HTTP URL of the CloWM website                                                                                         |
| `CLOWM_BLOCK_FOREIGN_USERS` | `block_foreign_users` | `false`       | boolean  | `false`                | Block users that have no role                                                                                         |

### Database

| Env variable           | Config file key | Default     | Value              | Example       | Description                                                    |
|------------------------|-----------------|-------------|--------------------|---------------|----------------------------------------------------------------|
| `CLOWM_DB__HOST`       | `db.host`       | `localhost` | <db hostname / IP> | `localhost`   | IP or Hostname Address of DB                                   |
| `CLOWM_DB__PORT`       | `db.port`       | 3306        | Integer            | 3306          | Port of the database                                           |
| * `CLOWM_DB__USER`     | `db.user`       | unset       | String             | `db-user`     | Username of the database user                                  |
| * `CLOWM_DB__PASSWORD` | `db.password`   | unset       | String             | `db-password` | Password of the database user                                  |
| * `CLOWM_DB__NAME`     | `db.name`       | unset       | String             | `db-name`     | Name of the database                                           |
| `CLOWM_DB__VERBOSE`    | `db.verbose`    | `false`     | Boolean            | `false`       | Enables verbose SQL output.<br>Should be `false` in production |

### S3

| Env variable                            | Config file key                  | Default   | Value    | Example                    | Description                                                                      |
|-----------------------------------------|----------------------------------|-----------|----------|----------------------------|----------------------------------------------------------------------------------|
| * `CLOWM_S3__URI`                       | `s3.uri`                         | unset     | HTTP URL | `http://localhost`         | URI of the S3 Object Storage                                                     |
| * `CLOWM_S3__ACCESS_KEY`                | `s3.acess_key`                   | unset     | String   | `ZR7U56KMK20VW`            | Access key for the S3 that owns the buckets                                      |
| * `CLOWM_S3__SECRET_KEY`                | `s3.secret_key`                  | unset     | String   | `9KRUU41EGSCB3H9ODECNHW`   | Secret key for the S3 that owns the buckets                                      |
| * `CLOWM_S3__USERNAME`                  | `s3.username`                    | unset     | String   | `clowm-bucket-manager`     | ID of the user in ceph who owns all the buckets. Owner of `CLOWM_S3__ACCESS_KEY` |
| * `CLOWM_S3__ADMIN_ACCESS_KEY`          | `s3.admin_acess_key`             | unset     | String   | `ZR7U56KMK20VW`            | Access key for the Ceph Object Gateway user with `user=*,bucket=*` capabilities. |
| * `CLOWM_S3__ADMIN_SECRET_KEY`          | `s3.admin_secret_key`            | unset     | String   | `9KRUU41EGSCB3H9ODECNHW`   | Secret key for the Ceph Object Gateway user with `user=*,bucket=*` capabilities. |
| `CLOWM_S3__INITIAL_BUCKET_SIZE_LIMIT`   | `s3.initial_bucket_size_limit`   | `100 GiB` | ByteSize | `10 KB`, `10 KiB`, `10 MB` | Size limit of the initial bucket. Between `1 KiB` and `4.3 TB`                   |
| `CLOWM_S3__INITIAL_BUCKET_OBJECT_LIMIT` | `s3.initial_bucket_object_limit` | `10000`   | Integer  | `10000`                    | Number of object limit in the initial bucket. Must be $<2^{32}$                  |

### Security

| Env variable                                   | Config file key                    | Default            | Value                           | Example            | Description                                                                                   |
|------------------------------------------------|------------------------------------|--------------------|---------------------------------|--------------------|-----------------------------------------------------------------------------------------------|
| `CLOWM_PRIVATE_KEY` / `CLOWM_PRIVATE_KEY_FILE` | `private_key` / `private_key_file` | randomly generated | Public Key / Path to Public Key | `/path/to/key`     | Private part of RSA Key in PEM format to sign JWTs. Public part is inferred from private key. |
| `CLOWM_JWT_TOKEN_EXPIRE_MINUTES`               | `jwt_token_expire_minutes`         | 8 days             | number                          | 11520              | Minutes till a JWT expires                                                                    |
| `CLOWM_SECRET_KEY`                             | `secret_key`                       | randomly generated | string                          | `xxxx`             | Secret key to sign Session Cookies                                                            |
| * `CLOWM_OPA__URI`                             | `opa.uri`                          | unset              | HTTP URL                        | `http://localhost` | URI of the OPA Service                                                                        |

### Lifescience OIDC

| Env variable                              | Config file key                   | Default                                  | Value    | Example                                  | Description                                                                 |
|-------------------------------------------|-----------------------------------|------------------------------------------|----------|------------------------------------------|-----------------------------------------------------------------------------|
| * `CLOWM_LIFESCIENCE_OIDC__CLIENT_ID`     | `lifescience_oidc.client_id`      | unset                                    | string   | `xxx`                                    | OIDC Client secret                                                          |
| * `CLOWM_LIFESCIENCE_OIDC__CLIENT_SECRET` | `lifescience_oidc.client_secret`  | unset                                    | string   | `xxx`                                    | OIDC Client ID                                                              |
| `CLOWM_LIFESCIENCE_OIDC__BASE_URI`        | `lifescience_oidc.base_uri`       | `https://login.aai.lifescience-ri.eu`    | HTTP URL | `https://login.aai.lifescience-ri.eu`    | Lifescience OIDC Base URI. Will be concatenated with `CLOWM_META_INFO_PATH` |
| `CLOWM_LIFESCIENCE_OIDC__META_INFO_PATH`  | `lifescience_oidc.meta_info_path` | `/oidc/.well-known/openid-configuration` | string   | `/oidc/.well-known/openid-configuration` | Path to the OIDC meta data file                                             |

### Monitoring

| Env variable                | Config file key      | Default | Value   | Example     | Description                                                                                  |
|-----------------------------|----------------------|---------|---------|-------------|----------------------------------------------------------------------------------------------|
| `CLOWM_OTLP__GRPC_ENDPOINT` | `otlp.grpc_endpoint` | unset   | String  | `localhost` | OTLP compatible endpoint to send traces via gRPC, e.g. Jaeger. If unset, no traces are sent. |
| `CLOWM_OTLP__SECURE`        | `otlp.secure`        | `false` | Boolean | `false`     | Connection type                                                                              |

## Getting started

This service depends on multiple other services. See [DEVELOPING.md](DEVELOPING.md) how to set these up for developing
on your local machine.

## License

The API is licensed under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license. See
the [License](LICENSE) file for more information
